The taxonomy_assoc module lets you display a node - along with the usual node listing for that term - when you view a taxonomy term. The node can be used instead of (or as well as) the description that taxonomy_context outputs. Unlike raw descriptions, however, nodes can be commented on, can have filters applied to them, and can be used in a number of other ways.

This module also includes a filter that inserts into a post a list of terms that are children of that post's associated term. Custom list items can be appended to the generated list of terms using a special syntax.


INSTALLATION

"taxonomy_assoc.module" and "taxonomy_assoc.css" should be placed in the modules folder. (Alternatively, the entire "taxonomy_assoc" folder can be uploaded with the module and css files in it.)

You must run the database script in "taxonomy_assoc.mysql" in order for this module to work.

Taxonomy_assoc.module must be activated via 'administer/modules'. 


FILTER

The taxonomy_assoc filter should be activated for each input format that should be able to use it (input filters are edited via 'administer/input formats'). 

The filter should only really be activated for input formats that administrators have access to. It is not appropriate, in most situations, for general users to be using this filter.


FORMAT

To insert a list of child terms into a node, simply enter the following:

[assoc /]

The more advanced syntax allows custom list items to be appended to the generated list of terms:

[assoc]Custom item 1,path/to/item1;Custom item 2,path/to/item2[/assoc]


THEME

There are two css rules located in "taxonomy_assoc.css" which can be altered to change the way associated nodes are displayed.

'assoc' controls the display of the whole node, when viewed inside a taxonomy term. Because the node is displayed in the help text area, it is generally subject to the smaller font size of help text. This style increases the font size to try and cancel out this effect.
'ul.assoc-list ' controls the display of the list itself.
